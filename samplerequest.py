{
	"route": "/where/to/route/request",
	"action": "CREATE/READ/UPDATE/DELETE",  # or other user defined action
	"meta": {
		"request-id": "base64uuid",
		"some": "other user defined stuff"
	},
	"data": {
		"the": "actual payload to send"
	}
}
