import trio
import asyncio
import cbor2 as cbor
from guid import GUID
from pynng import Req0


class Client:
	def __init__(self, *args, **kwargs):
		self.proto = kwargs.get('protocol', 'tcp')
		self.host = kwargs.get('host', '127.0.0.1')
		self.port = kwargs.get('port', '1212')
		self.address = f'{self.proto}://{self.host}:{self.port}'
		if len(args) >= 1:
			self.address = args[0]

	async def send(self, data, route='/', action='GET', meta=None):
		meta = meta or {}
		request = dict(
			route=route,
			action=action,
			meta={
				'request-id': GUID(),
				**meta
			},
			data=data
		)
		with Req0(dial=self.address) as socket:
			await socket.asend(cbor.dumps(request))
			print(f"sent request: {request}")
			response = cbor.loads(await socket.arecv())
			print(f"received response: {response}")


async def main():
	clients = [Client('tcp://127.0.0.1:1123') for i in range(1)]
	async with trio.open_nursery() as nursery:
		for i, client in enumerate(clients):
			print(f"starting client {i}")
			nursery.start_soon(client.send, {'hello': f'from {i}'})


if __name__ == '__main__':
	trio.run(main)
