import anyio
import cbor2 as cbor
from guid import GUID
from pynng import Rep0


class Server:
	def __init__(self, *args, **kwargs):
		self.proto = kwargs.get('protocol', 'tcp')
		self.host = kwargs.get('host', '127.0.0.1')
		self.port = kwargs.get('port', '1123')
		self.address = f'{self.proto}://{self.host}:{self.port}'
		if len(args) >= 1:
			self.address = args[0]
		self.routes = self.build_routes(kwargs.get('routes', {}))
		print(f"self.routes: {self.routes}")

	def route(self, *args, **kwargs):
		pass

	def build_routes(self, routes):
		_routes = {}
		for key, value in routes.items():
			_routes[key] = value
		return _routes

	def get_route(self, route):
		return self.routes[route]

	async def handle_request(self, context, request):
		ctx_id = context.context.id
		print(f"{ctx_id} receieved request: {request}")
		route = request['route']
		print(f"route: {route}")
		method = self.get_route(route)
		# If no route, set status to 405 and return rsp
		print(f"method: {method}")
		print("doing work")
		request_id = request.get('meta', {}).get('request-id', GUID())
		_response = {
			'route': route,
			'action': request['action'],
			'meta': {
				'request-id': request_id,
			},
			'data': {}
		}
		response = await method(request, _response)
		response = response or _response
		print("work done")
		await context.asend(cbor.dumps(response))
		print(f"sent response: {response}")
		context.close()
		print(f"context {ctx_id} closed")

	async def run(self):
		async with anyio.create_task_group() as tg:
			with Rep0(listen=self.address) as socket:
				while True:
					context = socket.new_context()
					request = cbor.loads(await context.arecv())
					tg.start_soon(self.handle_request, context, request)


class Endpoint:
	def create(self, req, res):
		pass

	def read(self, req, res):
		pass

	def update(self, req, res):
		pass

	def delete(self, req, res):
		pass


if __name__ == '__main__':
	#@server.route('/', ['GET'])
	async def route1(req, res):
		print(f"req: {req}")
		print(f"res: {res}")
		print("route1")
		await anyio.sleep(4)
		res['data'] = {'message': 'hello from route1'}

	server = Server(
		'tcp://127.0.0.1:1123',
		routes={
			'/': route1,
			#'/admin/': {
			#	'/secret': 'SecretsEndpoint'
			#},
			#'/workflow/job/<job-id>/logs': None
		}
	)

	anyio.run(server.run)
